# GPSfix

I created this library because I noticed that my Garmin watch's GPS data is quite imprecise, which messes up my activity data on Runalyze. So, I only use Garmin's heartrate and cadence data, merge it with the GPS data from my phone's Strava tracking, and upload that to Runalyze.

Also, I had some fun testing out the different APIs.

## Quick run

In your dedicated Python environment, run 
```
python main.py yyyy-mm-dd yyyy-mm-dd --auth_file path/to/auth_file
```
where the two `yyyy-mm-dd` are the start and stop dates between which to search for activities, and `path/to/auth_file` should point to a json file containting the paths to your strava, garmin and runalyze authentication files.

## Dependencies

This project relies on several really helpful libraries, particularly for API interactions:

* [`tcxreader`](https://github.com/alenrajsp/tcxreader): Reader for Garmin's TCX file format
* [`gpxpy`](https://github.com/tkrajina/gpxpy): A python GPX parser
* [`stravalib`](https://github.com/stravalib/stravalib): Provides simple client interface to Strava's REST API
* [`garminconnect`](https://github.com/cyberjunky/python-garminconnect): API wrapper for Garmin Connect to get activity statistics

**Note**: There are some conflicting dependendies: On pip install of `garminconnect`, the package `pydantic` is installed in a newer version that breaks the compatibility with `stravalib` (which officially requires `pydantic==1.10.9`).

My Workaround: After installing `stravalib` and `garminconnect`, do:
```
pip install pydantic==1.10.12
```
This works with `stravalib` (even though not according to its official requirements) AND with `garminconnect` (i.e. the underlying `garth` package).

## API setup

I store the credentials for API access to Strava, Garmin and Runalyze in JSON-files on my computer, and the main file of this package loads it from there. The paths to these files are defined in 'auth_paths.json'. So all you need to do is put your own credentials in JSON-files (find templates for how they should look in the directory 'authentication_templates') and make the paths in that file point to the correct location.