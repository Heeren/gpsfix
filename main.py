#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 16:34:03 2023

@author: paul
"""

from pathlib import Path
import argparse
import json
#import sys

#current_dir = Path(__file__).parent.resolve()
#sys.path.append(current_dir)

from src import StravaActivity, GarminActivity, StravaConnector, GarminConnector, GPSFixer, RunalyzeConnector


"""
class ActivityHandler():
    
    def __init__(self, start_date: str=None, stop_date: str=None, 
                 activity_type: str='Run', garmin_auth: Path=None,
                 strava_auth: Path=None)
"""


def fix_activities():
    # Set up the parser for input arguments
    parser = argparse.ArgumentParser(
            description='Combine data from activities')
    
    # Required input arguments:
    parser.add_argument('start_date', type=str, help='Search for activities after this date.')
    parser.add_argument('stop_date', type=str, help='Search for activities before this date.')
    parser.add_argument('--garmin', type=str, help='Path to the Garmin authentication file.')
    parser.add_argument('--strava', type=str, help='Path to the Strava authentication file.')
    parser.add_argument('--runalyze', type=str, help='Path to the runalyze authentication file.')
    parser.add_argument('--auth_file', type=str, help='Path to a json file with the paths to the strava, garmin & runalyze authentication files.')
    
    # Parse the input arguments
    args = parser.parse_args()
    
    start_date = args.start_date
    stop_date = args.stop_date
    if args.auth_file:
        with open(args.auth_file, 'r') as f:
            auth_paths = json.load(f)
        strava_auth   = auth_paths['strava']
        garmin_auth   = auth_paths['garmin']
        runalyze_auth = auth_paths['runalyze']
    else:
        strava_auth   = args.strava
        garmin_auth   = args.garmin
        runalyze_auth = args.runalyze

    # The Garmin connection and activity retrieval
    garmin_connect = GarminConnector()
    garmin_connect.load_auth_dict(garmin_auth)
    garmin_connect.setup_client()
    garmin_connect.get_type_activities(after=start_date, before=stop_date)
    print(f'\nNumber of activities found in Garmin: {len(garmin_connect.activities)}')
    for activity in garmin_connect.activities.values():
        print(f'\t{activity["start_time"]}')
    
    # The Strava connection and activity retrieval
    strava_connect = StravaConnector()
    strava_connect.load_auth_dict(strava_auth)
    strava_connect.refresh_tokens() #check_and_refresh_tokens()
    strava_connect.write_auth_file(strava_auth)
    strava_connect.get_type_activities(after=start_date, before=stop_date, limit=100, append=False)
    print(f'\nNumber of activities found in Strava: {len(strava_connect.activities)}')
    for activity in strava_connect.activities.values():
        print(f'\t{activity["start_time"]}')
    
    # Loop over activities and match between strava and garmin
    print('\nMatching activities:')
    matched_activities = []

    for garmin_id in garmin_connect.activities.keys():
        start_time_garmin = garmin_connect.activities[garmin_id]['start_time']
        print(f'\nGarmin: [{garmin_id}, {start_time_garmin}]')

        strava_id, start_time_strava = strava_connect.closest_activity(start_time_garmin)

        if not strava_id:
            print('Strava: No matching activity found!')
            strava_id = -1
            start_time_strava = '0000-00-00T00:00:00.0'
        
        elif strava_id in [act['strava_id'] for act in matched_activities]:
            # ToDo: What to do in this case???
            print(f'Strava: [{strava_id}, {start_time_strava}] <-- already existent!!!')
        
        else:
            print(f'Strava: [{strava_id}, {start_time_strava}]')

        matched_activities.append(
            {
                'garmin_id': garmin_id,
                'strava_id': strava_id,
                'start_time_garmin': start_time_garmin,
                'start_time_strava': start_time_strava
            }
        )

    # Now loop over each activity with valid match-up
    for i in range(len(matched_activities)):
        if matched_activities[i]['strava_id'] == -1:
            continue

        print('\n----------\nWorking on activity match:\n\t\tid\t\tstart_time')
        print(f'Garmin\t{matched_activities[i]["garmin_id"]}\t{matched_activities[i]["start_time_garmin"]}')
        print(f'Strava\t{matched_activities[i]["strava_id"]}\t{matched_activities[i]["start_time_strava"]}\n')

        # Garmin activity streams
        garmin_connect.get_activity(matched_activities[i]['garmin_id'])
        garmin_connect.save_tcx(Path('../gpsfix/data/garmin/activity_test.tcx'), overwrite=True)
        
        # Strava activity streams
        strava_connect.get_activity(matched_activities[i]['strava_id'])
        strava_connect.get_activity_streams(identifier=matched_activities[i]['strava_id'])
        
        # Setup Garmin and Strava activity
        garmin_act = GarminActivity(Path('../gpsfix/data/garmin/activity_test.tcx'))
        strava_act = StravaActivity(details=strava_connect.activity_dict, streams=strava_connect.activity_streams)
        
        # Setup the GPS fixer and interpolate the data
        gpx_fix = GPSFixer(garmin_activity=garmin_act, strava_activity=strava_act)
        gpx_fix.find_overlapping_times()
        gpx_fix.interpolate_garmin_cadence(only_overlap=True)
        gpx_fix.interpolate_garmin_heartrate(only_overlap=True)
        gpx_fix.create_gpx()
        gpx_fix.save_gpx(output_file=Path('../gpsfix/data/strava/activity_test_fixed.gpx'), overwrite=True)
        
        # Connect to runalyze and upload
        runalyze_connect = RunalyzeConnector()
        runalyze_connect.load_auth_dict(runalyze_auth)
        runalyze_connect.upload_activity(Path('../gpsfix/data/strava/activity_test_fixed.gpx'))


if __name__ == '__main__':
    fix_activities()