#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 15:32:11 2023

@author: paul
"""

from setuptools import setup, find_packages

import gpsfix


setup(
    name = 'gpsfix',
    version = gpsfix.__version__,
    description = 'Library for combining activity data from Garmin and Strava',
    license = 'MIT License',
    author = 'Paul Heeren',
    author_email = 'ppheeren@posteo.de',
    url = 'https://gitlab.com/Heeren/gpsfix',
    packages = find_packages(),
    install_requires=[
        # List your third-party dependencies here
        'third-party-package1',
        'third-party-package2',
        # Add other dependencies as needed
    ],
)
