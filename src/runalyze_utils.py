#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 17 22:00:24 2023

@author: paul
"""

import requests
import json
from pathlib import Path

RUNALYZE_API_ENDPOINT = 'https://runalyze.com/api/v1/'


class RunalyzeConnector():
    
    def __init__(self):
        self.auth_dict = None
        
    def load_auth_dict(self, auth_file: Path=None):
        if auth_file:
            with open(auth_file, 'r') as f:
                self.auth_dict = json.load(f)
        else:
            print('No authentication filename provided.')
    
    def upload_activity(self, filepath: Path, print_response: bool=True):
        
        r = requests.post(RUNALYZE_API_ENDPOINT + 'activities/uploads', 
                          files={'file': open(filepath, 'rb')}, 
                          headers=self.auth_dict)
        
        if print_response:
            print(r.content)