# Standard libraries
import numpy as np
#import matplotlib.pyplot as plt
from pathlib import Path

from scipy.interpolate import splrep, splev
from datetime import datetime

# gpxpy and tcxreader
import gpxpy
#import gpxpy.gpx

# Library to add extensions
try:
    # Load LXML or fallback to cET or ET 
    import lxml.etree as mod_etree  # type: ignore
except:
    try:
        import xml.etree.cElementTree as mod_etree # type: ignore
    except:
        import xml.etree.ElementTree as mod_etree # type: ignore


from . import StravaActivity, GarminActivity
    
    

class GPSFixer():
    
    def __init__(self, garmin_activity: GarminActivity, strava_activity: StravaActivity):
        self.garmin = garmin_activity
        self.strava = strava_activity
        self.start_time, self.stop_time = None, None
        self.strava_inds = np.arange(0, len(self.strava.times)-1)
        self.garmin_inds = np.arange(0, len(self.garmin.times)-1)
        
        self.gpx = None
        
        
    def find_overlapping_times(self):
        self.start_time = np.max([np.min(self.strava.times), np.min(self.garmin.times)])
        self.stop_time  = np.min([np.max(self.strava.times), np.max(self.garmin.times)])
        self.garmin_inds = np.where(
                (self.garmin.times >= self.start_time) & 
                (self.garmin.times <= self.stop_time))[0]   
        self.strava_inds = np.where(
                (self.strava.times >= self.start_time) & 
                (self.strava.times <= self.stop_time))[0]
    
    
    def interpolate_data(self, times1: np.array, data1: np.array, times2: np.array,
                         s: int=1, der: int=0):
        tck = splrep(times1.astype('d'), data1, s=s)
        data2 = splev(times2.astype('d'), tck, der=der)
        return data2
    
    
    def interpolate_garmin_heartrate(self, s: int=1, der: int=0, only_overlap: bool=True):
        
        if only_overlap:
            if not self.start_time or not self.stop_time:
                self.find_overlapping_times()
            
            strava_heartrates = np.zeros(len(self.strava.times))
            strava_heartrates[self.strava_inds] = self.interpolate_data(
                    self.garmin.times[self.garmin_inds], self.garmin.heartrates[self.garmin_inds],
                    self.strava.times[self.strava_inds], s=s, der=der)
            
        else:
            strava_heartrates = self.interpolate_data(
                    self.garmin.times, self.garmin.heartrates,
                    self.strava.times, s=s, der=der)
        
        self.strava.add_waypoints_param(name='heartrates', data=strava_heartrates)
    
    
    def interpolate_garmin_cadence(self, s: int=1, der: int=0, only_overlap: bool=True):
        
        if only_overlap:
            if not self.start_time or not self.stop_time:
                self.find_overlapping_times()
            
            strava_cadences = np.zeros(len(self.strava.times))
            strava_cadences[self.strava_inds] = self.interpolate_data(
                    self.garmin.times[self.garmin_inds], self.garmin.cadences[self.garmin_inds],
                    self.strava.times[self.strava_inds], s=s, der=der)
            
        else:
            strava_cadences = self.interpolate_data(
                    self.garmin.times, self.garmin.cadences,
                    self.strava.times, s=s, der=der)
        
        self.strava.add_waypoints_param(name='cadences', data=strava_cadences)
    
    
    def setup_gpx_header(self, only_overlap: bool=True):
        
        #self.gpx = gpxpy.gpx.GPX()
        if not self.gpx:
            raise ValueError('GPX object not initialized yet!')
        
        # Basic information
        self.gpx.name = self.strava.header_info('name')
        #self.gpx.description = 'Marrekrite aanlegplaatsen'
        #self.gpx.type = self.strava.header_info('type')
        if only_overlap:
            self.gpx.time = self.start_time.astype(datetime)
        else:
            self.gpx.time = self.strava.header_info('time')
        
        # Definition of extension
        namespace = '{gpxtpx}'
        # Add extension to header
        nsmap = {namespace[1:-1]: 'http://www.garmin.com/xmlschemas/TrackPointExtension/v1'}
        self.gpx.nsmap = nsmap
    
    
    def setup_gpx_waypoints(self, only_overlap: bool=True):
        
        if not self.gpx:
            raise ValueError('GPX object not initialized yet!')
        
        track = gpxpy.gpx.GPXTrack()
        track_segment = gpxpy.gpx.GPXTrackSegment()
        
        if only_overlap:
            inds = self.strava_inds
        else:
            inds = range(len(self.strava.waypoints['time']))
        
        track_points = []
        for i in inds:
            track_point = gpxpy.gpx.GPXTrackPoint()
            # Add the gps, time and elevation data
            track_point.latitude  = self.strava.latitudes[i]
            track_point.longitude = self.strava.longitudes[i]
            track_point.time      = self.strava.times_as_datetime[i]
            track_point.elevation = self.strava.altitudes[i]
            
            # Create extension element
            root = mod_etree.Element('{gpxtpx}TrackPointExtension')
            # Add heartrate and cadence data
            root_hr  = mod_etree.SubElement(root, '{gpxtpx}hr')
            root_cad = mod_etree.SubElement(root, '{gpxtpx}cad')
            root_hr.text  = str(int(self.strava.waypoints['heartrates'][i]))
            root_cad.text = str(int(self.strava.waypoints['cadences'][i]))
            # Append extension to trackpoint
            track_point.extensions.append(root)
            # Append trackpoint
            track_points.append(track_point)
        
        # Trackpoints to segment
        track_segment.points = track_points
        # Append segment to track
        track.segments = [track_segment]
        # Set type to 'running'
        track.type = 'running'
        # Append track to GPX tracks
        self.gpx.tracks.append(track)
    
    
    def create_gpx(self, only_overlap: bool=True):
        
        self.gpx = gpxpy.gpx.GPX()
        
        self.setup_gpx_header(only_overlap=only_overlap)
        self.setup_gpx_waypoints(only_overlap=only_overlap)
        
    
    def save_gpx(self, output_file: Path, overwrite: bool=False):
        
        if not self.gpx:
            raise ValueError('GPX object not initialized yet!')
            
        if output_file.is_dir():
            raise TypeError('No filename provided!')
        
        if output_file.exists() and not overwrite:
            raise ValueError('File already exists!')
        
        with open(output_file, 'w') as f:
            f.write(self.gpx.to_xml())