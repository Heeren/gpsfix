#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  7 16:12:57 2023

@author: paul
"""

from garminconnect import Garmin
import json
from pathlib import Path
from tcxreader.tcxreader import TCXReader
import numpy as np
from datetime import datetime, timedelta


class GarminConnector():
    
    def __init__(self):
        self.auth_dict = None
        self.client = None
        
        self.activity_tcx = None
        self.activities = {}
        
    def load_auth_dict(self, auth_file: Path=None):
        if auth_file:
            with open(auth_file, 'r') as f:
                self.auth_dict = json.load(f)
        else:
            print('No authentication filename provided.')
    
    def write_auth_file(self, auth_file: Path=None):
        if auth_file:
            with open(auth_file, 'w') as f:
                json.dump(self.auth_dict, f, indent=0)
        else:
            print('No authentication filename provided.')
        
    def setup_client(self):
        self.client = Garmin(self.auth_dict['email'], self.auth_dict['password'])
        self.client.login()
    
    """
    Garmin query stuff
    """
    
    def get_activity(self, identifier: int):
        self.activity_tcx = self.client.download_activity(identifier).decode("utf-8") 
    
    def get_type_activities(self, activitytype: str='running', after: str=None, before: str=None, append=True):
        if not append:
            self.activities = {}
        
        for activity in self.client.get_activities_by_date(startdate=after, enddate=before, activitytype=activitytype):
            self.activities[activity['activityId']] = {
                'activity': activity,
                'start_time': datetime.strptime(activity['startTimeGMT'], '%Y-%m-%d %H:%M:%S')
            }
    
    """
    Return activity id closest to a specific date
    """
    def closest_activity(self, check_date: datetime):
        if len(self.activities) == 0:
            raise IndexError('No activities recorded.')
        
        if isinstance(check_date, str):
            check_date = datetime.strptime(check_date, '%Y-%m-%d %H:%M:%S')
        
        closest_id = None
        closest_start_time = None
        min_timedelta = timedelta(seconds=86400.)
        for id, data in self.activities.items():
            start_time = data['start_time']
            if abs(start_time - check_date) < min_timedelta:
                closest_id = id
                closest_start_time = start_time
                min_timedelta = abs(start_time - check_date)
        
        return closest_id, closest_start_time
    
    """
    I/O stuff
    """
    
    def save_tcx(self, output_file: Path, overwrite: bool=False):
        if output_file.is_dir():
            raise TypeError('No filename provided!')
        
        if output_file.exists() and not overwrite:
            raise ValueError('File already exists!')
        
        with open(output_file, 'w') as f:
            f.write(self.activity_tcx)



class GarminActivity():
    
    
    def __init__(self, input_file: Path):
        #self.input_file = input_file
        self.tcx_reader = TCXReader()
        self.read_tcx(input_file)
    
    def read_tcx(self, input_file):
        self.activity = self.tcx_reader.read(input_file, only_gps=False)
        
    @property
    def longitudes(self):
        return np.array([point.longitude for point in self.activity.trackpoints])
    
    @property
    def latitudes(self):
        return np.array([point.latitude for point in self.activity.trackpoints])
    
    @property
    def times(self):
        return np.array([point.time for point in self.activity.trackpoints], dtype='datetime64')
    
    @property
    def times_as_datetime(self):
        return [point.time for point in self.activity.trackpoints]
    
    @property
    def heartrates(self):
        return np.array([point.hr_value for point in self.activity.trackpoints])
    
    @property
    def cadences(self):
        return np.array([point.tpx_ext['RunCadence'] for point in self.activity.trackpoints])
        
        