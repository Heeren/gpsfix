#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 26 16:24:07 2023

@author: paul
"""

from stravalib import Client
import json
from pathlib import Path
#import requests
import numpy as np
from datetime import timedelta, datetime
import time


class StravaConnector():
    
    def __init__(self):
        self.client = Client()
        
        self.auth_dict = None
        self.redirect_uri = 'http://127.0.0.1:5000/authorization'
        # Authentication code hardwired for now
        #self.auth_code = '27b0c8b63bb6329306b32f7ed0887e9373d2c021'
        
        self.activity = None
        self.activity_streams = None
        self.activities = {}
        
    """
    Setup stuff
    """
    def load_auth_dict(self, auth_file: Path=None):
        if auth_file:
            with open(auth_file, 'r') as f:
                self.auth_dict = json.load(f)
        else:
            print('No authentication filename provided.')
    
    def write_auth_file(self, auth_file: Path=None):
        if auth_file:
            with open(auth_file, 'w') as f:
                json.dump(self.auth_dict, f, indent=0)
        else:
            print('No authentication filename provided.')
    
    def get_auth_code(self):
        url = self.client.authorization_url(
            client_id=self.auth_dict['client_id'], redirect_uri=self.redirect_uri, 
            scope=['read_all','profile:read_all','activity:read_all'])
        print(url)
        
        # ToDo: Use the requests module somehow to retrieve the authentication code?!
        #
        # self.auth_dict['auth_code'] = '27b0c8b63bb6329306b32f7ed0887e9373d2c021' #request.args.get('code') # e.g.
        
    def get_tokens(self):
        token_response = self.client.exchange_code_for_token(
            client_id=self.auth_dict['client_id'],
            client_secret=self.auth_dict['client_secret'],
            code=self.auth_dict['auth_code'])
        print(token_response)
        self.auth_dict['access_token'] = token_response['access_token']
        self.auth_dict['refresh_token'] = token_response['refresh_token']  # You'll need this in 6 hours
    
    def refresh_tokens(self):
        token_response = self.client.refresh_access_token(
            client_id=self.auth_dict['client_id'],
            client_secret=self.auth_dict['client_secret'],
            refresh_token=self.auth_dict['refresh_token'])
        self.auth_dict['access_token'] = token_response['access_token']
        self.auth_dict['refresh_token'] = token_response['refresh_token']
        self.auth_dict['expires_at'] = token_response['expires_at']
    
    def token_expired(self):
        expired = False
        if time.time() > self.auth_dict['expires_at']:
            print('Token has expired!')
        return expired
    
    def check_and_refresh_tokens(self):
        if self.token_expired():
            self.refresh_tokens()
    
    """
    Strava query stuff
    """
    def get_activity(self, identifier: int):
        self.activity = self.client.get_activity(identifier)
    
    def get_type_activities(self, type: str='Run', after: str=None, before: str=None, limit: int=None, append=True):
        if not append:
            self.activities = {}
        
        for activity in self.client.get_activities(after=after, before=before, limit=limit):
            if activity.type == type:
                self.activities[activity.id] = {
                    'activity': activity,
                    'start_time': activity.start_date.replace(tzinfo=None)
                }
    
    def get_activity_streams(self, identifier: int, types: list=['time', 'latlng', 'altitude', 'distance'],# 'velocity_smooth', 'moving'],
                             series_type='time', resolution='high'):
        
        self.activity_streams = self.client.get_activity_streams(
            identifier, types=types, series_type=series_type, resolution=resolution)
    

    """
    Return activity id closest to a specific date
    """
    def closest_activity(self, check_date: [datetime, str]):
        if len(self.activities) == 0:
            raise IndexError('No activities recorded.')
        
        if isinstance(check_date, str):
            check_date = datetime.strptime(check_date, '%Y-%m-%d %H:%M:%S')

        closest_id = None
        closest_start_time = None
        min_timedelta = timedelta(seconds=86400.)
        for id, data in self.activities.items():
            start_time = data['start_time']
            if abs(start_time - check_date) < min_timedelta:
                closest_id = id
                closest_start_time = start_time
                min_timedelta = abs(start_time - check_date)
        
        return closest_id, closest_start_time
    
    """
    Data transformation stuff
    """
    
    @property
    def activity_dict(self):
        return self.activity.to_dict()
    
    



class StravaActivity():
    
    general_keys = {
        'name': 'name',
        'time': 'start_date',
        'type': 'type'
    }
    """
    waypoint_keys = {
        'lat': 'latitudes',
        'lon': 'longitudes',
        'ele': 'altitudes',
        'time': 'times',
    }
    """
    def __init__(self, details: dict, streams: dict):
        self.details = details
        self.streams = streams
        
        self.setup_waypoints()
    
    def convert_times(self):
        #if 'time' not in self.streams.keys():
        #    raise KeyError('The streams do not contain "time"!')
        
        times = []
        for i in range(len(self.streams['time'].data)):
            times.append(self.details['start_date'] + timedelta(seconds=self.streams['time'].data[i]))
        #times = np.array(times, dtype='datetime64')
        return times
    
    def transform_latlng(self):
        #if 'latlng' not in self.streams.keys():
        #    raise KeyError('The streams do not contain "latlng"!')
        
        latlng = {}
        latlng['latitudes'] = np.array(self.streams['latlng'].data)[:,0]
        latlng['longitudes'] = np.array(self.streams['latlng'].data)[:,1]
        return latlng
    
    def setup_waypoints(self):
        self.waypoints = {}
        
        for key in self.streams.keys():
            if key == 'latlng':
                latlng = self.transform_latlng()
                self.waypoints.update(latlng)
            elif key == 'time':
                self.waypoints['times'] = self.convert_times()
            else:
                self.waypoints[key+'s'] = np.array(self.streams[key].data)
    
    def add_waypoints_param(self, name: str, data: np.array):
        self.waypoints[name] = data
    
    @property
    def longitudes(self):
        return self.waypoints['longitudes']
    
    @property
    def latitudes(self):
        return self.waypoints['latitudes']
    
    @property
    def times_as_datetime(self):
        return self.waypoints['times']
    
    @property
    def times(self):
        return np.array(self.times_as_datetime, dtype='datetime64')
    
    @property
    def altitudes(self):
        return self.waypoints['altitudes']
    
    def header_info(self, gpx_key: str):
        return self.details[self.general_keys[gpx_key]]
    
    @property
    def waypoint_keys(self):
        return list(self.waypoints.keys())
    